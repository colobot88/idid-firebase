// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBDPuwUCEhHkMiEXldUK8ZMCTqNv-SHJeI",
    authDomain: "ididfirebasetest.firebaseapp.com",
    databaseURL: "https://ididfirebasetest.firebaseio.com",
    projectId: "ididfirebasetest",
    storageBucket: "ididfirebasetest.appspot.com",
    messagingSenderId: "589777344339"
  }
};
