import { Injectable, EventEmitter } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';

declare var google;

@Injectable()
export class IdidMapService {

    private http: HttpClient;
    private currentLocation: any;
    locationChanged: EventEmitter<any> = new EventEmitter();
    zoomChanged: EventEmitter<any> = new EventEmitter();

    constructor(http: HttpClient) {
        this.http = http;
    }

    getPlacesByLatLng(lat: any, lng: any): Observable<any> {
        var location = new google.maps.LatLng(lat, lng);    

        var request = {
            location: location,
            radius: 20,
        };
        let service = new google.maps.places.PlacesService(document.createElement('div'));
        return service.nearbySearch(request, function(places: any){
            console.log(places);
        });
    }

    setCurrentLocation(location: any) {
        this.currentLocation = location;
        this.locationChanged.emit(this.currentLocation);
    }
    
    setZoom(zoom: number) {
        this.zoomChanged.emit(zoom);
    }
}