import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import { TranslateService } from '@ngx-translate/core';

export interface Post {
    createdAt: firebase.firestore.FieldValue,
    authorId: any;
    verbs: any[];
    location: firebase.firestore.GeoPoint;
    placeId: any;
}

@Injectable()
export class PostService {

    constructor(private translate: TranslateService, public afAuth: AngularFireAuth, private afs: AngularFirestore) {

    }

    getPost(authorId: string, placeId: string): Promise<any> {
        return this.afs.collection("posts").ref
            .where("authorId", "==", authorId)
            .where("placeId", "==", placeId).get();
    }

    sendPost(authorId: any, verbs: any[], lat: any, lng: any, placeId: any): Promise<any> {
        // let verbIds = [];
        // for(let i = 0; i < verbs.length; i++) {
        //     verbIds.push(verbs[i].id);
        // }
        let post: Post = {
            createdAt: firebase.firestore.FieldValue.serverTimestamp(),
            authorId: authorId,
            verbs: verbs,
            location: new firebase.firestore.GeoPoint(lat, lng),
            placeId: placeId,
        }

        return this.afs.collection("posts").add(post);
        
        // .then(function() {
        //     console.log("Document for new post successfully written!");
        // });
    }
}