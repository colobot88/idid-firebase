import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import { TranslateService } from '@ngx-translate/core';

export interface Verb {
    en: string;
    tr: boolean;
}

@Injectable()
export class VerbService {

    //
    verbCollectionRef: AngularFirestoreCollection<Verb>;
    verb$: Observable<Verb[]>;
    verbs: any;
    //

    constructor(private translate: TranslateService, public afAuth: AngularFireAuth, private afs: AngularFirestore) {

    }

    getVerbsFromDb() {
        // this.afs.collection("verbs").ref.get().then(function(querySnapshot) {
        //     querySnapshot.forEach(function(doc) {
        //         // doc.data() is never undefined for query doc snapshots
        //         console.log(doc.id, " => ", doc.data());
        //     });
        // });
    }

    getVerbs() {
        // return this.verb$;
    }

    search(searchValue: string, extension: string, lang: string = this.translate.currentLang): Observable<{}[]> {
        let self = this;
        var strSearch = searchValue;
        var strlength = strSearch.length;
        var strFrontCode = strSearch.slice(0, strlength-1);
        var strEndCode = strSearch.slice(strlength-1, strSearch.length);

        var startcode = strSearch;
        var endcode= strFrontCode + String.fromCharCode(strEndCode.charCodeAt(0) + 1);

        return self.afs.collection(`verbs`, ref => ref
            .where(lang + '.' + extension, '>=', startcode)
            .where(lang + '.' + extension, '<', endcode)
            .limit(10))
            .valueChanges();
        // return self.afs.collection(`verbs`, ref => ref
        //     .orderBy("en")
        //     .startAt(searchValue.toLowerCase())
        //     .endAt(searchValue.toLowerCase() + "\uf8ff")
        //     .limit(10))
        //     .valueChanges();
    }
}