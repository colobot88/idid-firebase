import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';

export interface User {
    uid: string;
    email: string;
    photoURL?: string;
    displayName?: string;
    postIds?: any[];
    favoriteColor?: string;
}

export interface Verb {
  en: string;
  tr: boolean;
}

@Injectable()
export class AuthService {

    private initialized: boolean = false;
    private token: any;

    authState: Observable<firebase.User>
    private currentUser: firebase.User = null;

    user: Observable<User>;

    constructor(public afAuth: AngularFireAuth, private afs: AngularFirestore) {
        this.authState = this.afAuth.authState;
        this.authState.subscribe(user => {
            if (user) {
                this.currentUser = user;
                this.afs.doc<User>(`users/${user.uid}`).valueChanges()
            } else {
                this.currentUser = null;
            }
            this.initialized = true;
        });
    }

    loginWithFacebook() {
        this.initialized = false;
        return this.afAuth.auth.signInWithPopup(
            new firebase.auth.FacebookAuthProvider()
        )
            .then((res) => {
                this.updateUserData(res.user)
                this.token = res.credential.accessToken;
                this.initialized = true;
            })
            .catch((err) => {
                this.initialized = true;
                this.currentUser = null;
                console.log(err)
            });
    }


    private updateUserData(user) {
        // Sets user data to firestore on login
        const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
        const data: User = {
            uid: user.uid,
            email: user.email,
            displayName: user.displayName,
            photoURL: user.photoURL
        }
        return userRef.set(data)
    }

    logout() {
        return this.afAuth.auth.signOut();
    }

    isLoggedIn() {
        if (!this.initialized) {
            return null;    // still waiting response
        }
        if (this.currentUser == null) {
            return false;   // user is not logged in
        }
        return true;    // user is logged in
    }

    getCurrentUser() {
        return this.currentUser;
    }

    updateUser(user: User, postId: any): Promise<any> {
        var userRef = this.afs.collection("users").doc(user.uid);
        return userRef.collection("posts").add({postId: postId});
    }
}