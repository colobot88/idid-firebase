﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';

import { AutoCompleteComponent } from './autocomplete.component';
import { SpinnerModule } from '../spinner/spinner.module';

@NgModule({
    imports: [CommonModule, FormsModule, SpinnerModule, AgmCoreModule.forRoot({
        libraries: ['places']
    }) ],
  declarations: [AutoCompleteComponent],
  exports: [AutoCompleteComponent]
})

export class AutoCompleteModule { }
