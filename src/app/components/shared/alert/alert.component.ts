import { Component, Input, Output, ElementRef, EventEmitter, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'idid-alert',
    templateUrl: 'alert.component.html',
    styleUrls: ['alert.component.css']
})
export class AlertComponent {

    @Input() title: string = 'Title';
    @Input() message: string = 'message';
    @Input() buttonText: string = 'OK';

    public visible: boolean = false;

    show() {
        this.visible = true;
    }

    hide() {
        this.visible = false;
    }
}
