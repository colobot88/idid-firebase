﻿import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { AutoCompleteModule } from './autocomplete/autocomplete.module';
import { AutoCompleteComponent } from './autocomplete/autocomplete.component';
import { GoogleMapsModule } from './google-maps/google-maps.module';
import { GoogleMapsComponent } from './google-maps/google-maps.component';
import { ToolbarModule } from './toolbar/toolbar.module';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { SpinnerModule } from './spinner/spinner.module';
import { TagsModule } from './tags/tags.module';
import { TagsComponent } from './tags/tags.component';
import { SelectModule } from './select/select.module';
import { SelectComponent } from './select/select.component';
import { AlertModule } from './alert/alert.module';
import { AlertComponent } from './alert/alert.component';

@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule, TranslateModule, AutoCompleteModule, GoogleMapsModule, ToolbarModule, SpinnerModule, TagsModule,
        SelectModule, AlertModule
    ],
    declarations: [
        // COMPONENTS
        // PIPES
    ],
    providers: [
        // FILTERS
        // PIPES
        // SERVICES
    ],
    exports: [
        CommonModule, FormsModule, RouterModule, TranslateModule,
        // COMPONENTS
        AutoCompleteComponent, GoogleMapsComponent, ToolbarComponent, TagsComponent, SelectComponent, AlertComponent
    ]
})

export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule
        };
    }
}