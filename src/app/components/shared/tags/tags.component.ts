import { Component, Input, Output, ElementRef, EventEmitter, ViewChild, OnInit, NgZone } from '@angular/core';
import { VerbService } from '../../../services/verb.service';

@Component({
    selector: 'idid-tags',
    host: {
        '(document:click)': 'handleClick($event)',
        '(keydown)': 'handleBlur($event)',
        '(document:keydown)': 'keyDown($event)'
    },
    templateUrl: 'tags.component.html',
    styleUrls: ['./tags.component.css']
})
export class TagsComponent implements OnInit {

    @ViewChild('cont') contEl: any;
    @ViewChild('input') inputEl: any;
    @ViewChild("myInput") private _inputElement: ElementRef;

    @Input() selectedVerbs: any[] = [];
    @Input() lang: string = 'en';

    extension: string = 'past';

    @Output() selectedVerbsChange = new EventEmitter<any>();

    selectedIdx: number = 0;
    public query = '';
    public filteredList = [];
    searching: boolean = false;
    public elementRef;
    private verbService: VerbService;


    constructor(verbService: VerbService, myElement: ElementRef) {
        this.elementRef = myElement;
        this.verbService = verbService;
    }

    ngOnInit() {
        console.log(this._inputElement);
    }

    handleClick(event) {
        var clickedComponent = event.target;
        var inside = false;
        do {
            if (clickedComponent === this.elementRef.nativeElement) {
                inside = true;
            }
            clickedComponent = clickedComponent.parentNode;
        } while (clickedComponent);
        if (!inside) {
            this.filteredList = [];
        }
        this.selectedIdx = 0;
    }

    focus() {
        this._inputElement.nativeElement.focus();
    }

    search() {
        if (!this.query || this.query === '') {
            this.filteredList = [];
            return;
        }
        this.searching = true;
        this.verbService.search(this.query.toLowerCase(), 'past').subscribe(
            (results)=> {
                console.log(results);
                this.filteredList = results;
                this.searching = false;
        },
        (error)=>{
            this.searching = false;
        })
    }

    scrollList() {
        let current = this.contEl.nativeElement.scrollTop;
        let targetLi: HTMLElement = document.getElementById(this.selectedIdx + '');
        this.contEl.nativeElement.scrollTop = 128; // targetLi.offsetTop;
    }

    keyDownReady: boolean = true;
    keyDown(event: any) {
        if (event.code == 'Backspace') {
            if (!this.query || this.query === '') {
                if (this.selectedVerbs && this.selectedVerbs.length > 0) {
                    this.removeVerb(this.selectedVerbs[this.selectedVerbs.length - 1]);
                }
            }
            return;
        }

        if (this.filteredList.length > 0 && (event.code == "ArrowDown" || event.code == "ArrowUp")) {
            event.preventDefault();
        }
        if (this.keyDownReady) {
            if (event.code == "ArrowDown" && this.selectedIdx < this.filteredList.length - 1) {
                event.preventDefault();
                this.keyDownReady = false;
                setTimeout(() => {
                    this.keyDownReady = true;
                    this.selectedIdx++;
                    this.scrollUL(this.selectedIdx);
                }, 100);
            } else if (event.code == "ArrowUp" && this.selectedIdx > 0) {
                event.preventDefault();
                this.keyDownReady = false;
                setTimeout(() => {
                    this.keyDownReady = true;
                    this.selectedIdx--;
                    this.scrollUL(this.selectedIdx);
                }, 100);
            }
        }
    }

    scrollUL(li) {
        // scroll UL to make li visible
        // li can be the li element or its id
        if (typeof li !== "object") {
            li = document.getElementById(li);
        }
        if (!li) {
            return;
        }
        var ul = li.parentNode;
        if (!ul) {
            return;
        }
        // fudge adjustment for borders effect on offsetHeight
        var fudge = 4;
        // bottom most position needed for viewing
        var bottom = (ul.scrollTop + (ul.offsetHeight - fudge) - li.offsetHeight);
        // top most position needed for viewing
        var top = ul.scrollTop + fudge;
        if (li.offsetTop <= top) {
            // move to top position if LI above it
            // use algebra to subtract fudge from both sides to solve for ul.scrollTop
            ul.scrollTop = li.offsetTop - fudge;
        } else if (li.offsetTop >= bottom) {
            // move to bottom position if LI below it
            // use algebra to subtract ((ul.offsetHeight - fudge) - li.offsetHeight) from both sides to solve for ul.scrollTop
            ul.scrollTop = li.offsetTop - ((ul.offsetHeight - fudge) - li.offsetHeight);
        }
    };

    select(item) {
        this.query = this.getLocalizedValue(item, this.lang, this.extension);
        this.filteredList = [];
        this.selectedIdx = 0;
        if (!this.selectedVerbs) {
            this.selectedVerbs = [];
        }
        this.selectedVerbs.push(item);
        this.selectedVerbsChange.emit(this.selectedVerbs);
        this.query = '';
        let __this = this;
        setTimeout(function() { 
            __this.focus();
        }, 100);
    }

    removeVerb(verb) {
        if (!this.selectedVerbs) {
            this.selectedVerbs = [];
        }
        let index = this.selectedVerbs.indexOf(verb, 0);
        if (index > -1) {
            this.selectedVerbs.splice(index, 1);
        }
        // for(let i = 0; i < this.selectedVerbs.length; i++) {
        //     if (this.selectedVerbs[i] === verb)
        //     {
        //         this.selectedVerbs = this.selectedVerbs.splice(i, i + 1);
        //     }
        // }
        this.selectedVerbsChange.emit(this.selectedVerbs);
    }

    onResetClick() {
        this.query = '';
    }

    handleBlur(key: any) {
        if (!key || key.keyCode === 13 || key.keyCode === 9) {
            if (this.selectedIdx > -1 && this.filteredList && this.filteredList.length > 0) {
                // this.query = this.getLocalizedValue(this.filteredList[this.selectedIdx], this.lang);
                this.select(this.filteredList[this.selectedIdx]);

                if (key.keyCode === 13) {
                    let __this = this;
                    setTimeout(function() { 
                        __this.focus();
                    }, 100);
                }
            }
            this.filteredList = [];
            // this.selectedIdx = -1;
            let inputEl: HTMLElement = document.getElementById('input');
            inputEl.blur();
        }
    }

    getLocalizedValue(item: any, lang: string, extension: string) {
        if(!item) {
            return '';
        }
        return item[lang][extension];
    }
}