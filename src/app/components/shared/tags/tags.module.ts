import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TagsComponent } from './tags.component';
import { SpinnerModule } from '../spinner/spinner.module';

@NgModule({
    imports: [ CommonModule, FormsModule, SpinnerModule ],
  declarations: [TagsComponent],
  exports: [TagsComponent]
})

export class TagsModule { }
