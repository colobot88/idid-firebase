﻿import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { AgmCoreModule } from '@agm/core';

import { GoogleMapsComponent } from './google-maps.component';
import { MapContentComponent } from './map-content/map-content.component';

@NgModule({
    imports: [
        CommonModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyDjTo5WrhSZel5ZpHtEZ1nCoJ17T3_htn0',
            libraries: ["places"]
        })],
    exports: [GoogleMapsComponent],
    declarations: [GoogleMapsComponent, MapContentComponent],
    providers: [],
})
export class GoogleMapsModule { }
