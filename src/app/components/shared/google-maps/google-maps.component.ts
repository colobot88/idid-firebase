﻿import { Component, Input, Output, EventEmitter, ViewChild, HostListener, AfterViewInit, NgZone } from '@angular/core';
import { AgmMap } from '@agm/core';
import { IdidMapService } from '../../../services/ididmap.service';

declare function require(path: string);

@Component({
    selector: 'idid-google-maps',
    templateUrl: './google-maps.component.html',
    styleUrls: ['./google-maps.component.css']
})
export class GoogleMapsComponent implements AfterViewInit {

    @ViewChild('AgmMap') myMap: AgmMap;

    @Input('lat') lat: any;
    @Input('lng') lng: any;
    @Input('zoom') zoom: number = 2;

    @Output() zoomChange = new EventEmitter<any>();
    @Output() onNewMarkerClick = new EventEmitter<any>();

    markerNewPosition: any = null;
    mouseDown: boolean = false;

    // myIcon: any = {
    //   url: require('../../../../assets/icons/svg/placeholder-new.svg'), // gives a data://<value>
    //   scaledSize: {
    //     height: 40,
    //     width: 40
    //   }
    // };

    constructor(private _ngZone: NgZone, ididMapService: IdidMapService) {
        ididMapService.locationChanged.subscribe((place) => {
            this.onLocationChanged(place);
        });
        // this.myMap.mapReady.subscribe(
        //     (ready) => {
        //         this.myMap.centerChange.subscribe(
        //             (data) => {
        //                 console.log(data);
        //             }
        //         )
        //     }
        // )
    }

    ngAfterViewInit() {
        this.myMap.disableDefaultUI = true;
        this.myMap.usePanning = true;
        this.myMap.mapReady.subscribe(
            (ready) => {

                // this._ngZone.runOutsideAngular(() => {
                //     this._increaseProgress(() => {
                //         // reenter the Angular zone and display done
                //         this._ngZone.run(() => { console.log('Outside Done!'); });
                //     });
                // });


                this.myMap.centerChange.subscribe(
                    (data) => {
                        this.markerNewPosition = {};
                        this.markerNewPosition.lat = data.lat;
                        this.markerNewPosition.lng = data.lng;
                    }
                )
            }
        )
    }

    @HostListener('window:resize')
        onWindowResize() {
        this.myMap.triggerResize();
    }

    @HostListener('document:mouseup')
    onMouseup() {
        this.mouseDown = false;
    }

    @HostListener('document:mousemove', ['$event'])
    onMousemove(event: MouseEvent) {

    }

    @HostListener('document:mousedown', ['$event'])
    onMousedown(event) {
        this.mouseDown = true;
    }

    onLocationChanged(place: any) {
        if (place && place.geometry && place.geometry.location) {
            this.setCenter(place.geometry.location.lat(), place.geometry.location.lng());
        }
    }

    onZoomChange(newZoom: number) {
        this.zoomChange.emit(newZoom);
    }

    setCenter(lat: any, lng: any) {
        this.lat = lat;
        this.lng = lng;
        this.zoom = 15;
    }

    dragEnded() {
        // console.log('dragEnded');
    }

    newMarkerClicked() {
        let location = {
            lat: this.myMap.latitude,
            lng: this.myMap.longitude
        }
        this.onNewMarkerClick.emit(location);
    }
}
