﻿import { Component, Input, Output, ElementRef, EventEmitter, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'idid-spinner',
    templateUrl: 'spinner.component.html',
    styleUrls: ['spinner.component.css']
})
export class SpinnerComponent {
    @Input() size: string = '32px';
    @Input() color: string = 'white';
}
