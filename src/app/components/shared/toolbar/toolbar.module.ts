﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { AutoCompleteModule } from '../autocomplete/autocomplete.module';
import { SpinnerModule } from '../spinner/spinner.module';
import { ToolbarComponent } from './toolbar.component';

@NgModule({
    imports: [CommonModule, FormsModule, TranslateModule, AutoCompleteModule, SpinnerModule],
    exports: [ToolbarComponent],
    declarations: [ToolbarComponent],
    providers: []
})
export class ToolbarModule { }
