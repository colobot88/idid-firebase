﻿import { Component, Input, Output, ElementRef, EventEmitter, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'idid-toolbar',
    templateUrl: 'toolbar.component.html',
    styleUrls: ['toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

    @Input('user') user: any;
    @Input('lang') lang: string = 'en';
    @Input('loggedin') loggedin: boolean = false;

    @Output('onSearchClicked') onSearchClicked: EventEmitter<any> = new EventEmitter();
    @Output('on-place-select') onPlaceSelect: EventEmitter<any> = new EventEmitter();
    @Output('onFBLoginClicked') onFBLoginClicked: EventEmitter<any> = new EventEmitter();
    @Output('onFBLogoutClicked') onFBLogoutClicked: EventEmitter<any> = new EventEmitter();

    lat: number = 51.678418;
    lng: number = 7.809007;

    translate: TranslateService;

    constructor(private _translate: TranslateService, private router: Router) {
        this.translate = _translate;
        console.log(this.translate);
    }

    ngOnInit() {
        console.log(this.user);
    }

    navigate(route: string) {
        this.router.navigate([route]);
    }

    onPlaceSelected(place: any) {
        console.log(place);
        this.onPlaceSelect.emit(place);
    }

    searchClicked() {
        this.onSearchClicked.emit();
        // this.router.navigate(['home']);
    }

    loginWithFacebook() {
        this.onFBLoginClicked.emit();
        //this.loggedin = true;
    }

    logout() {
        this.onFBLogoutClicked.emit();
        //this.loggedin = false;
        //this.router.navigate(['home']);
    }
}
