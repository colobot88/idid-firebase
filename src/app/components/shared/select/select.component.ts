import { Component, Input, Output, ElementRef, EventEmitter, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'idid-select',
    templateUrl: 'select.component.html',
    styleUrls: ['select.component.css']
})
export class SelectComponent implements OnInit {

    @Input() selectedItem: any;
    @Input() items: any[] = [];
    @Input() field: string;

    @Output() selectedItemChange = new EventEmitter<any>();
    
    expanded: boolean = false;

    ngOnInit() {
        // this.selectedItem = this.items ? this.getItemValue(this.items[0]) : null;
    }

    blur() {
        let __this = this;
        setTimeout(function() {
            __this.expanded = false;
        }, 200);
    }

    selectItem(item) {
        this.selectedItem = item;
        // this.blur();
        this.selectedItemChange.emit(item);
    }

    getItemValue(item: any) {
        if (!item) {
            return null;
        }
        if (this.field) {
            return item[this.field];
        } else {
            return item;
        }
    }
}
