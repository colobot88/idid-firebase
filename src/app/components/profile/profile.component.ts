﻿import { Component } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
    selector: 'profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})
export class ProfileComponent {

    constructor(private authService: AuthService) {

    }

    getCurrentUser() {
        return this.authService.getCurrentUser();
    }
}
