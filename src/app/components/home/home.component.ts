import { Component, Input, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { User } from '../../services/auth.service';
import { IdidMapService } from '../../services/ididmap.service';
import { PostService } from '../../services/post.service';
import { TranslateService } from '@ngx-translate/core';
import { AlertComponent } from '../shared/alert/alert.component';

declare var google;

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {

    @ViewChild('tooFarAlert') tooFarAlert: AlertComponent;
    @ViewChild('noPlaceFound') noPlaceFound: AlertComponent;

    selectedPlace: any;
    selectedPost: any;

    lat: number = 0; // 57.70928429068325;
    lng: number = 0; // 2.5488866677590005;
    zoom: number = 2;

    ididMapService: IdidMapService;
    postService: PostService;
    translate: TranslateService;
    popupOpen: boolean = false;

    selectedVerbs: any[];

    places: any[];
    field: string = 'name';

    mapReady: boolean = false;
    browserLocation: any;

    constructor(private _translate: TranslateService, private authService: AuthService, ididMapService: IdidMapService, postService: PostService) {
        this.translate = _translate;
        this.postService = postService;
        this.ididMapService = ididMapService;
        this.ididMapService.zoomChanged.subscribe((zoom) => {
            this.zoom = zoom;
        });
    }

    ngOnInit() {
        navigator.geolocation.getCurrentPosition((position) => {
            this.mapReady = true;
            this.browserLocation = position.coords;
            this.lat = position.coords.latitude;
            this.lng = position.coords.longitude;
            this.zoom = 17;
        }, (error) => {
            this.mapReady = true;
        })
    }

    ngAfterViewInit() {
        console.log(this.tooFarAlert);
    }

    onPlaceSelect(place: any) {
        console.log(place);
        this.lat = place.geometry.location.lat();
        this.lng = place.geometry.location.lng();
        this.zoom = 14;
    }
    
    onNewMarkerClick(location: any) {
        if (this.zoom < 13) {
            this.tooFarAlert.show();
            return;
        }

        this.selectedPlace = null;
        let __this = this;
        this.popupOpen = true;
        this.selectedVerbs = [];
        var location = new google.maps.LatLng(location.lat, location.lng);    

        var request = {
            location: location,
            radius: 15,
        };
        let service = new google.maps.places.PlacesService(document.createElement('div'));

        this.selectedPost = null;
        return service.nearbySearch(request, function(places: any){
            console.log(places);
            __this.places = places;
            __this.selectedPlace = places[0];
            __this.postService.getPost(__this.authService.getCurrentUser().uid, __this.selectedPlace.place_id).then(
                (post) => {
                    console.log(post);

                    post.forEach(function(doc) {
                        // doc.data() is never undefined for query doc snapshots
                        console.log(doc.id, " => ", doc.data());

                        if (!__this.selectedPost) {
                            __this.selectedPost = doc.data();
                        } else {
                            __this.selectedPost.verbs = __this.selectedPost.verbs.concat(doc.data().verbs);
                        }

                    });
                    if (__this.selectedPost) {
                        __this.selectedVerbs = __this.selectedVerbs.concat(__this.selectedPost.verbs);
                    }
                },
                (error) => {
                    console.log(error);
                }
            )

            if (!places || places.length == 0) {
                __this.popupOpen = false;
                __this.noPlaceFound.show();
            }
        });
        // this.ididMapService.getPlacesByLatLng(location.lat, location.lng).subscribe(
        //     (success) => {
        //         console.log(success);
        //     },
        //     (error) => {
        //         console.log('something went wrong while getting places');
        //     }
        // )
    }

    closePopup() {
        this.popupOpen = false;
    }

    onSelectedItemChange(place: any) {
        console.log(place);
        this.selectedVerbs = [];
        this.selectedPost = null;
            this.selectedPlace = place;

            let __this = this;
            this.postService.getPost(this.authService.getCurrentUser().uid, this.selectedPlace.place_id).then(
                (post) => {
                    console.log(post);

                    post.forEach(function(doc) {
                        // doc.data() is never undefined for query doc snapshots
                        console.log(doc.id, " => ", doc.data());

                        if (!__this.selectedPost) {
                            __this.selectedPost = doc.data();
                        } else {
                            __this.selectedPost.verbs = __this.selectedPost.verbs.concat(doc.data().verbs);
                        }

                    });
                    if (__this.selectedPost) {
                        __this.selectedVerbs = __this.selectedVerbs.concat(__this.selectedPost.verbs);
                    }
                },
                (error) => {
                    console.log(error);
                }
            )
    }

    sharePost() {
        let lat = this.selectedPlace.geometry.location.lat();
        let lng = this.selectedPlace.geometry.location.lng();
        this.postService.sendPost(this.authService.getCurrentUser().uid, this.selectedVerbs, lat, lng, this.selectedPlace.place_id ).then((data) => {
            console.log('post saved -> ' + data);
            let user: User = this.authService.getCurrentUser();
            if (!user.postIds){
                user.postIds = [];
            }
            user.postIds.push(data.id);
            this.authService.updateUser(user, data.id).then(
                (success) => {
                    console.log('user updated');
                },(error) => {
                    console.log(error);
                }
            );
        },
        (error) => {
            console.log('error while saving post -> ' + error);
        })
    }

    selectedVerbsChange(verbs: any) {
        console.log(verbs);
    }

}
