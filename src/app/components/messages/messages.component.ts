﻿import { Component, ViewChild, OnInit } from '@angular/core';

@Component({
    selector: 'messages',
    templateUrl: './messages.component.html',
    styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

    @ViewChild('chatBox') chatBox: any;

    leftPanelOpen: boolean = false;

    messages: any[] = [{ username: 'Ritesh', message: 'Hi, Genelia how are you and my son?', time: '10.00 am', gender: 'male', imageDir: 'images/users/ritesh.jpg', isCurrentUser: false },
        { username: 'Genelia', message: 'Hi, How are you Ritesh!!! We both are fine sweetu.', time: '10.03 am', gender: 'Female', imageDir: 'images/users/genu.jpg', isCurrentUser: true },
        { username: 'Ritesh', message: 'Oh great!!! just enjoy you all day and keep rocking', time: '10.05 am', gender: 'male', imageDir: 'images/users/ritesh.jpg', isCurrentUser: false },
        { username: 'Ritesh', message: 'hola!', time: '10.05 am', gender: 'male', imageDir: 'images/users/ritesh.jpg', isCurrentUser: false },
        { username: 'Ritesh', message: 'alooo!', time: '10.05 am', gender: 'male', imageDir: 'images/users/ritesh.jpg', isCurrentUser: false },
        { username: 'Genelia', message: 'Your movei was superb and your acting is mindblowing', time: '10.07 am', gender: 'Female', imageDir: 'images/users/genu.jpg', isCurrentUser: true },
        { username: 'Genelia', message: 'hop?', time: '10.07 am', gender: 'Female', imageDir: 'images/users/genu.jpg', isCurrentUser: true },
        { username: 'Genelia', message: 'hop?', time: '10.07 am', gender: 'Female', imageDir: 'images/users/genu.jpg', isCurrentUser: true },
        { username: 'Genelia', message: 'hop?', time: '10.07 am', gender: 'Female', imageDir: 'images/users/genu.jpg', isCurrentUser: true },
        { username: 'Genelia', message: 'hop?', time: '10.07 am', gender: 'Female', imageDir: 'images/users/genu.jpg', isCurrentUser: true }];

    constructor() {
    }

    ngOnInit() {
        setTimeout(() => {
            if (this.chatBox) {
                this.chatBox.nativeElement.scrollTop = this.chatBox.nativeElement.scrollHeight;
            }
        }, 1);
    }

    openClosePanel() {
        this.leftPanelOpen = !this.leftPanelOpen;
    }
}
