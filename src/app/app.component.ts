import { Component, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from './services/auth.service';
import { TranslateService } from '@ngx-translate/core';

import { IdidMapService } from './services/ididmap.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {

    title: string = 'My first AGM project';
    lat: number = 51.678418;
    lng: number = 7.809007;
    loggedin: boolean = false;

    param = {value: 'homepage'};
    
    translate: TranslateService;
    ididMapService: IdidMapService;

    constructor(private _translate: TranslateService, private authService: AuthService, private afAuth: AngularFireAuth, private router: Router, ididMapService: IdidMapService) {
        this.translate = _translate;
        this.translate.setDefaultLang('en');
        this.translate.use(this.translate.getBrowserLang());
        this.ididMapService = ididMapService;
    }

    @HostListener('document:keydown', ['$event'])
    handleKeyboardEvent(event: KeyboardEvent) {
        console.log(event);
        let x = event.keyCode;
        if (event.keyCode === 36) {
          if (this.translate.currentLang == 'en') {
            this.translate.use('tr');
          } else {
            this.translate.use('en');
          }
        }
    }

    onPlaceSelected(place: any) {
        console.log(place);
    }

    Countries: ['asd', 'bsd'];

    navigate(route: string) {
        this.router.navigate([route]);
    }

    onFBLoginClicked() {
        console.log('app.component onFBLoginClicked');
        this.authService.loginWithFacebook()
            .then((res) => {
                console.log(res);
            })
            .catch((err) => console.log(err));
    }

    onFBLogoutClicked() {
        console.log('app.component onFBLogoutClicked');
        this.authService.logout()
            .then((res) => {
                console.log(res);
                this.logout();
            })
            .catch((err) => console.log(err));
    }

    loginWithFacebook() {
        this.loggedin = true;
    }

    logout() {
        this.loggedin = false;
        this.router.navigate(['/home']);
    }

    isLoggedIn() {
        return this.authService.isLoggedIn();
    }

    getCurrentUser() {
        return this.authService.getCurrentUser();
    }

    onSearchClicked() {
        // this.ididMapService.setZoom(15);
        this.router.navigate(['home']);
    }
}
