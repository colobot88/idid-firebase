import { BrowserModule } from '@angular/platform-browser';
import { NgModule, HostListener } from '@angular/core';

import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AppComponent } from './app.component';

import { RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { MessagesComponent } from './components/messages/messages.component';
import { SharedModule } from './components/shared/shared.module';

import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from 'environments/environment';

import { AgmCoreModule } from '@agm/core';
import { FirebaseService } from './services/firebase.service';

// import { FirebaseTransLoader } from './helpers/FirebaseTransLoader';

import { AuthGuard } from './guards/auth.guard';
import { AuthService } from './services/auth.service';
import { VerbService } from './services/verb.service';
import { IdidMapService } from './services/ididmap.service';
import { PostService} from './services/post.service';

// export function FbTransLoaderFactory(afs: AngularFirestore) {
//   return new FirebaseTransLoader(afs);
// }
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, "/assets/i18n/" , ".json");
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProfileComponent,
    MessagesComponent
  ],
  imports: [
    SharedModule,
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: (HttpLoaderFactory),
          deps: [HttpClient]
      }
    }),
    AgmCoreModule,
    AngularFireModule.initializeApp(environment.firebase, 'ididApp'),
    AngularFireAuthModule,
    AngularFirestoreModule,
    RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
            { path: 'messages', component: MessagesComponent, canActivate: [AuthGuard] },
            { path: '**', redirectTo: 'home' }
        ])
  ],
  providers: [FirebaseService, AuthService, AuthGuard, VerbService, IdidMapService, PostService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
