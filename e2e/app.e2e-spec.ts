import { IdidCliPage } from './app.po';

describe('idid-cli App', () => {
  let page: IdidCliPage;

  beforeEach(() => {
    page = new IdidCliPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
